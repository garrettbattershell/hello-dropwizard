<h1>Hello Dropwizard</h1>
<p>
Simple REST service using Dropwizard framework.

Credit to <href a="http://blog.scottlogic.com/2016/01/05/java-microservices-with-dropwizard-tutorial.html">http://blog.scottlogic.com/2016/01/05/java-microservices-with-dropwizard-tutorial.html</href> 
for providing a tutorial that helped me understand and develop the basics.</p>
<br />
<h2>How to run this service</h2>

<h3>As a Jar</h3>
From root:
1. gradle shadowJar
2. java -jar build/libs/hello-dropwizard-<version>-all.jar server task-service.yml

This will expose the service to port 8080 and health checks on port 8081

<h3>With Docker</h3>
From root:
1. gradle shadowJar
2. docker build -t <dockerhubname>/hello-dropwizard:<version> .
3. docker run -d -p 8888:8080 -p 8881:8081 <dockerhubname>/hello-dropwizard:<version>

This will expose the service to port 8888 and health checks on port 8881.
<br />**Note: dockerhubname/ is optional for image name.** 
