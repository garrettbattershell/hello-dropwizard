package resource;

import model.Task;
import org.junit.Before;
import org.junit.Test;


import javax.ws.rs.core.Response;
import java.util.List;
import java.util.Optional;
import static org.junit.Assert.*;

/**
 * Created by Garrett on 8/13/2017.
 */
public class TaskResourceTest {
    TaskResource taskResource;
    private Response response;
    private static final String TALK = "Talk";

    @Before
    public void setUp() {
        taskResource  = new TaskResource(100);
        response = null;
    }

    @Test
    public void testTaskRetrieval() {
//        whenNoContentIsGiven();
//        thenResponseWasNotNull();
//        thenResponseWas200();
//        thenTaskListReturned();
//        thenAllTasksReturned();
    }

    @Test
    public void testTaskByContentRetrieval() {
//        whenTalkContentIsGiven();
//        thenResponseWasNotNull();
//        thenResponseWas200();
//        thenTalkTaskReturned();
    }

    @Test
    public void testMaxTaskExceeded() {

//        taskResource = new TaskResource(4);
    }

    private void thenTalkTaskReturned() {
        assertTrue("Entity is not a Task", response.getEntity() instanceof Task);
        Task expectedTask = new Task(2, "Talk");
        assertEquals("Returned Task does not match Talk Task", response.getEntity(), expectedTask);
    }

    private void whenTalkContentIsGiven() {
       response = taskResource.getTasks(Optional.of("Talk"));
    }

    private void thenTaskListReturned() {
        assertNotNull("Task list came back null", response.getEntity());
        assertTrue("Entity is not a list", response.getEntity() instanceof  List);
    }

    private void thenResponseWas200() {
        assertEquals("Status returned was not Ok(200)", Response.Status.OK.getStatusCode(), response.getStatus());
    }

    private void thenResponseWasNotNull() {
        assertNotNull("Response  came back null", response);
    }

    private void thenAllTasksReturned() {
        //assertEquals("Not all tasks were returned", TaskResource.tasks.size(), ((List<Task>) response.getEntity()).size());
    }

    private void whenNoContentIsGiven() {
        response = taskResource.getTasks(Optional.empty());
    }
}
