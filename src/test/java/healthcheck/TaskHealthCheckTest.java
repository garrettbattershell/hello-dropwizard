package healthcheck;

import com.codahale.metrics.health.HealthCheck;
import model.Task;
import org.junit.Test;
import org.junit.Before; 

import java.util.ArrayList;
import java.util.List;
import static org.junit.Assert.*;

/** 
* TaskHealthCheck Tester. 
* 
* @author Garrett Battershell
* @since <pre>Aug 13, 2017</pre> 
*/
public class TaskHealthCheckTest {

    private List<Task> tasks;
    private TaskHealthCheck taskHealthCheck;
    private HealthCheck.Result result;

    @Before
    public void before() throws Exception {
        tasks = new ArrayList<>();
    }

    @Test
    public void testHealthy() throws Exception {
        givenTasks();
        whenCheckIsCalled();
        thenHealthCheckIsHealthy();
    }

    @Test
    public void testNullTasks() throws Exception {
        givenNullTasks();
        whenCheckIsCalled();
        thenHealthCheckIsHealthy();
    }

    @Test
    public void testNoTasks() throws Exception {
        givenNoTasks();
        whenCheckIsCalled();
        thenHealthCheckIsHealthy();
    }

    private void givenTasks() {
        tasks.add(new Task(1, "Test"));
        taskHealthCheck = new TaskHealthCheck(tasks);
    }
    private void givenNoTasks() {
        taskHealthCheck = new TaskHealthCheck(new ArrayList<>());
    }
    private void givenNullTasks() {
        taskHealthCheck = new TaskHealthCheck(null);
    }

    private void whenCheckIsCalled() throws Exception {
        result = taskHealthCheck.check();
    }

    private void thenHealthCheckIsHealthy() {
        assertTrue("HealthCheck did not return healthy", result.isHealthy());
    }
    private void thenHealthCheckIsUnhealthy() {
        assertFalse("HealthCheck returned healthy", result.isHealthy());

    }




}