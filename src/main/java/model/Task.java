package model;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.bson.Document;

/**
 * Created by Garrett on 8/12/2017.
 */
public class Task extends Document{
    private long number;
    private String content;

    public Task() {
        //for Jackson dersialization
    }

    public Task(long number, String content) {
        this.number = number;
        this.content = content;
    }

    @JsonProperty
    public long getNumber() {
        return number;
    }

    @JsonProperty
    public String getContent() {
        return content;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Task task = (Task) o;

        if (number != task.number) return false;
        return content != null ? content.equals(task.content) : task.content == null;
    }

    @Override
    public int hashCode() {
        int result = (int) (number ^ (number >>> 32));
        result = 31 * result + (content != null ? content.hashCode() : 0);
        return result;
    }

    public String getPath() {
        return "" + this.number;
    }

}
