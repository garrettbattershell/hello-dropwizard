package application;

import configuration.TaskConfiguration;
import healthcheck.TaskHealthCheck;
import io.dropwizard.Application;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;
import model.Task;
import resource.TaskResource;

import java.util.List;
import java.util.Optional;

/**
 * Created by Garrett on 8/12/2017.
 */
public class TaskApplication extends Application<TaskConfiguration> {

    public static void main(String[] args) throws Exception {
        new TaskApplication().run(args);
    }

    @Override
    public String getName() {
        return "task-list-service";
    }

    @Override
    public void initialize(Bootstrap<TaskConfiguration> bootstrap) {
        // nothing to do yet
    }

    @Override
    public void run(TaskConfiguration configuration, Environment environment) throws Exception {
        final TaskResource resource = new TaskResource(configuration.getMaxLength());
        environment.jersey().register(resource);
        environment.healthChecks().register("tasks", new TaskHealthCheck((List<Task>) resource.getTasks(Optional.empty()).getEntity()));
    }
}
