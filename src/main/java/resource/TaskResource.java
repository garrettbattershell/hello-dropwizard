package resource;

import com.codahale.metrics.annotation.Timed;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;
import model.Task;
import org.bson.BSON;
import org.bson.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.lang.reflect.Method;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import com.mongodb.MongoClient;

import static com.mongodb.client.model.Filters.eq;


/**
 * Created by Garrett on 8/12/2017.
 */
@Path("/task")
@Produces(MediaType.APPLICATION_JSON)
public class TaskResource {
    Logger logger = LoggerFactory.getLogger(TaskResource.class);
    //host name must match mongo service defined in docker-compose
    private MongoClient mongoClient = new MongoClient("mongodb");

    @Context UriInfo uriInfo;

    public TaskResource(int maxLength) {

    }

    @GET
    @Timed
    public Response getTasks(@QueryParam("content") Optional<String> content) {
        List<Task> tasks = new ArrayList<>();
        MongoDatabase database = mongoClient.getDatabase("database");
        MongoCollection<Task> collection = database.getCollection("tasks", Task.class);
        MongoCursor<Task> cursor;
        if(content.isPresent()) {
            cursor = collection.find(eq("content", content.get())).iterator();
        } else {
            cursor = collection.find().iterator();
        }
        while(cursor.hasNext()) {
            tasks.add(cursor.next());
        }
        return Response.ok(tasks).build();
    }

    @POST
    public Response addTask(Task task) {
        MongoDatabase database = mongoClient.getDatabase("database");
        MongoCollection<Task> collection = database.getCollection("tasks", Task.class);
        collection.insertOne(task);
        URI uri = uriInfo.getBaseUriBuilder().path(this.getClass()).path(task.getPath()).build();

        return Response.created(uri).build();//.entity(getTaskByNumber(task.getNumber())).build();
    }

    @GET
    @Path("{number}")
    public Response getTaskByNumber(@PathParam("number") long number ) {

        MongoDatabase database = mongoClient.getDatabase("database");
        MongoCollection<Task> collection = database.getCollection("tasks", Task.class);

        Task task = collection.find(eq("number", number)).first();
        return Response.ok(task).build();

    }
}
