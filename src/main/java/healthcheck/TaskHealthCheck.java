package healthcheck;

import com.codahale.metrics.health.HealthCheck;
import model.Task;

import java.util.List;

/**
 * Created by Garrett on 8/12/2017.
 */
public class TaskHealthCheck extends HealthCheck {
    private List<Task> tasks;

    public TaskHealthCheck(List<Task> tasks) {
        this.tasks = tasks;
    }


    @Override
    protected Result check() throws Exception {

            return Result.healthy();
    }
}
