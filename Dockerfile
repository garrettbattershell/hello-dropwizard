FROM anapsix/alpine-java
MAINTAINER Garrett Battershell
COPY build/libs/hello-dropwizard-1.0.0-all.jar /home/hello-dropwizard-1.0.0-all.jar
COPY task-service.yml /home/task-service.yml
CMD ["java", "-jar", "/home/hello-dropwizard-1.0.0-all.jar", "server", "/home/task-service.yml"]